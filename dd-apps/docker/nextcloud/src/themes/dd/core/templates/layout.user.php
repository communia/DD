<?php
/*
#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
*/
	$api_url = "https://" . preg_replace('/^nextcloud\./', 'api.', $_SERVER['HTTP_HOST']);
	$avatar_url = "https://" . preg_replace('/^nextcloud\./', 'sso.', $_SERVER['HTTP_HOST']) . "/auth/realms/master/avatar-provider";
	$profile_url = "https://" . preg_replace('/^nextcloud\./', 'sso.', $_SERVER['HTTP_HOST']) . "/auth/realms/master/account";
	$showsettings = in_array($_['user_uid'], ['admin', 'ddadmin']) ? true : false;

	// Remove user menu Settings item for non-administrator users
	if(!array_key_exists('core_apps', $_["settingsnavigation"])){
		unset($_["settingsnavigation"]["settings"]);
	}
?>
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="<?php p($_['language']); ?>" data-locale="<?php p($_['locale']); ?>" >
	<head data-user="<?php p($_['user_uid']); ?>" data-user-displayname="<?php p($_['user_displayname']); ?>" data-requesttoken="<?php p($_['requesttoken']); ?>">
		<meta charset="utf-8">
		<title>
			<?php
				p(!empty($_['application'])?$_['application'].' - ':'');
				p($theme->getTitle());
			?>
		</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
		<?php if ($theme->getiTunesAppId() !== '') { ?>
		<meta name="apple-itunes-app" content="app-id=<?php p($theme->getiTunesAppId()); ?>">
		<?php } ?>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-title" content="<?php p((!empty($_['application']) && $_['appid'] != 'files')? $_['application']:$theme->getTitle()); ?>">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="theme-color" content="<?php p($theme->getColorPrimary()); ?>">
		<link rel="icon" href="<?php print_unescaped(image_path($_['appid'], 'favicon.ico')); /* IE11+ supports png */ ?>">
		<link rel="apple-touch-icon" href="<?php print_unescaped(image_path($_['appid'], 'favicon-touch.png')); ?>">
		<link rel="apple-touch-icon-precomposed" href="<?php print_unescaped(image_path($_['appid'], 'favicon-touch.png')); ?>">
		<link rel="mask-icon" sizes="any" href="<?php print_unescaped(image_path($_['appid'], 'favicon-mask.svg')); ?>" color="<?php p($theme->getColorPrimary()); ?>">
		<link rel="manifest" href="<?php print_unescaped(image_path($_['appid'], 'manifest.json')); ?>">
		<link rel="stylesheet" href="<?php p($api_url) ?>/css/font-awesome-4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php p($api_url) ?>/css/dd.css">
		<link rel="stylesheet" href="/themes/dd/core/ddstyles.css">
        <script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>" src="/themes/dd/core/js/navbar.js"></script>
		<?php emit_css_loading_tags($_); ?>
		<?php emit_script_loading_tags($_); ?>
		<?php print_unescaped($_['headers']); ?>
		<style>
			/*
			 * Move "New text document" to last position of file app plus menu
			 */
			.newFileMenu > ul:nth-child(1) > li:nth-child(3) {
				order: 1;
			}
			#header .header-right>div>.menu:after {
				display:none;
			}
			.ddicon {
				position: relative;
				left: 8px;
				top: 9px;
			}
			.ddiconcontainer {
				margin-right: 16px;
				width: 36px;
				height: 36px;
				background-color: #f0f0f0;
				position: relative;
				border-radius: 5px;
				left: -7px;
			}
			#expanddiv {
				padding: .5rem .7rem;
			}
		</style>
	</head>
	<body id="<?php p($_['bodyid']);?>">
	<?php include 'layout.noscript.warning.php'; ?>

		<?php foreach ($_['initialStates'] as $app => $initialState) { ?>
			<input type="hidden" id="initial-state-<?php p($app); ?>" value="<?php p(base64_encode($initialState)); ?>">
		<?php }?>

		<a href="#app-content" class="button primary skip-navigation skip-content"><?php p($l->t('Skip to main content')); ?></a>
		<a href="#app-navigation" class="button primary skip-navigation"><?php p($l->t('Skip to navigation of app')); ?></a>

		<div id="notification-container">
			<div id="notification"></div>
		</div>
		<header role="banner" id="header">
            
			<div id="navbar-logo" class="header-left">
				<a href="<?php print_unescaped(link_to('', 'index.php')); ?>"
					id="nextcloud">
					<img src="<?php p($api_url) ?>/img/logo.png" alt="" style="height: 39px;">
				</a>
			</div>

			<div id="navbar-nextcloud" class="header-right">      

				<div id="notifications"></div>
				<div id="unified-search"></div>
				<div id="contactsmenu">
					<div class="icon-contacts menutoggle" tabindex="0" role="button"
					aria-haspopup="true" aria-controls="contactsmenu-menu" aria-expanded="false">
						<span class="hidden-visually"><?php p($l->t('Contacts'));?></span>
					</div>
					<div id="contactsmenu-menu" class="menu"
						aria-label="<?php p($l->t('Contacts menu'));?>"></div>
				</div>
				<div id="settings">
					<div id="expand" tabindex="0" role="button" class="menutoggle"
						aria-label="<?php p($l->t('Settings'));?>"
						aria-haspopup="true" aria-controls="expanddiv" aria-expanded="false">
						<div class="avatardiv<?php if ($_['userAvatarSet']) {
				print_unescaped(' avatardiv-shown');
			} else {
				print_unescaped('" style="display: none');
			} ?>">
							<?php if ($_['userAvatarSet']): ?>
								<img alt="" width="32" height="32"
								src="<?php p($avatar_url)?>"
								>
							<?php endif; ?>
						</div>
						<div id="expandDisplayName" class="icon-settings-white"></div>
					</div>
					<nav class="settings-menu" id="expanddiv" style="display:none;"
						aria-label="<?php p($l->t('Settings menu'));?>">
					<ul>
						<li>						
							<a target="_blank" href="<?php p($profile_url)?>">
								<div class="ddiconcontainer">
									<i class="icon fa fa-user fa-fw ddicon"></i>
								</div>
								<?php p($_['user_displayname']); ?>
							</a>
						</li>	
					<?php foreach ($_['settingsnavigation'] as $entry):?>
						<?php if ($showsettings) { ?>
							<li data-id="<?php p($entry['id']); ?>">
								<a href="<?php print_unescaped($entry['href']); ?>"
									<?php if ($entry["active"]): ?> class="active"<?php endif; ?>>
									<img alt="" src="<?php print_unescaped($entry['icon'] . '?v=' . $_['versionHash']); ?>">
									<?php p($entry['name']) ?>
								</a>
							</li>
						<?php } else if ($entry['id'] == 'logout') { ?>
							<li data-id="<?php p($entry['id']); ?>">
								<a href="<?php print_unescaped($entry['href']); ?>">
									<div class="ddiconcontainer">
										<i class="icon fa fa-sign-out fa-fw ddicon" aria-hidden="true"></i> </div>
										<?php p($entry['name']) ?>
								</a>
							</li>
						<?php } ?>																									
				<?php endforeach; ?>					
					</ul>
					</nav>
				</div>
				<div id="product-logo">
					<a href="https://xnet-x.net/ca/digital-democratic/" target="_blank">
						<img src="/themes/dd/core/img/dd.svg" alt="" style="height: 16px; margin-right: 21px; margin-top: 16px"/>
					</a>
				</div>
			</div>
		</header>

		<div id="sudo-login-background" class="hidden"></div>
		<form id="sudo-login-form" class="hidden" method="POST">
			<label>
				<?php p($l->t('This action requires you to confirm your password')); ?><br/>
				<input type="password" class="question" autocomplete="new-password" name="question" value=" <?php /* Hack against browsers ignoring autocomplete="off" */ ?>"
				placeholder="<?php p($l->t('Confirm your password')); ?>" />
			</label>
			<input class="confirm" value="<?php p($l->t('Confirm')); ?>" type="submit">
		</form>

		<div id="content" class="app-<?php p($_['appid']) ?>" role="main">
			<?php print_unescaped($_['content']); ?>
		</div>

	</body>
</html>
