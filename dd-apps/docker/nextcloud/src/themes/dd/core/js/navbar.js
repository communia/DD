/*
*   Copyright © 2021,2022 IsardVDI S.L.
*   Copyright © 2022 Evilham <contact@evilham.com>
*
*   This file is part of DD
*
*   DD is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or (at your
*   option) any later version.
*
*   DD is distributed in the hope that it will be useful, but WITHOUT ANY
*   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
*   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
*   details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with DD. If not, see <https://www.gnu.org/licenses/>.
*
* SPDX-License-Identifier: AGPL-3.0-or-later
*/
document.addEventListener("DOMContentLoaded", () => {
    base_url = `${window.location.protocol}//${window.location.host.replace(/^nextcloud\./, 'api.')}`
    $.getJSON(`${base_url}/json`, (result) => {
        if (result.logo) {
            $("#navbar-logo img").attr('src', result.logo)
        }
        if (result.product_logo) {
            $("#product-logo img").attr("src", result.product_logo)
        }
        if (result.product_url) {
            $("#product-logo a").attr("href", result.product_url)
        }
    })
    $.get(`${base_url}/header/html/nextcloud`, (result) => {
        $("#settings").before(result)
        $('#dropdownMenuAppsButton').click(() => {
            $('#dropdownMenuApps').toggle()
        })
        $('#dropdownMenuApps a').click(() => {
            $('#dropdownMenuApps').toggle()
        })
    })
    $(window).click( (event) => {
        if (
            !$(event.target).parents(
                '#dropdownMenuAppsButton, #dropdownMenuApps'
            ).length
        ) {
            $('#dropdownMenuApps').hide()
        }
    })
    $(window).blur( (event) => {
        $('#dropdownMenuApps').hide()
    })
})
