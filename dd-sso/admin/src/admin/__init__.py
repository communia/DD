#
#   Copyright © 2021,2022 IsardVDI S.L.
#   Copyright © 2022 Evilham <contact@evilham.com>
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import logging as log
import os
import os.path

from admin.flaskapp import AdminFlaskApp

def get_app() -> AdminFlaskApp:
    app = AdminFlaskApp(__name__, template_folder="static/templates")

    """
    Debug should be removed on production!
    """
    if app.debug:
        log.warning("Debug mode: {}".format(app.debug))
    else:
        log.info("Debug mode: {}".format(app.debug))

    return app


"""
Import all views
"""
from .views import ApiViews, AppViews, LoginViews, WebViews, WpViews
