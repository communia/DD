# Integracions

El DD es pot integrar amb altres sistemes a través de les seves APIs.

## Autenticació

Totes les peticions han d'estar autenticades amb un [Json Web Token (JWT)][jwt],
que estigui signat per l'`API_SECRET` (present al fitxer `dd.conf`).

Aquesta autenticació es fa mitjançant la capcelera HTTP `Authentication`.

<details>
<summary>Vegeu-ne els detalls</summary>
```sh
> curl -H "Authorization: bearer ${jwt}" https://admin.DOMAIN/ddapi/roles
[
  {
    "keycloak_id": "9325ad99-7e04-4c31-9768-5512e1564160",
    "id": "admin",
    "name": "admin",
    "description": "${role_admin}"
  },
  {
    "keycloak_id": "c6c8a73e-51fc-4716-831d-1dfc0e0b62b0",
    "id": "manager",
    "name": "manager",
    "description": "Realm managers"
  },
  {
    "keycloak_id": "24d7977e-da83-4591-8e13-0fac3126afa1",
    "id": "student",
    "name": "student",
    "description": "Realm students"
  },
  {
    "keycloak_id": "d6699c41-13d5-4623-bdca-e5f2775474ed",
    "id": "teacher",
    "name": "teacher",
    "description": "Realm teachers"
  }
]
```

On el <code>JWT</code> es pot generar, per exemple fent servir <code>python-jose</code>, de la
següent manera:

```python
import os
from jose import jws
t = jws.sign({}, os.environ["API_SECRET"], algorithm="HS256")
print(t)
```

Altres llenguatges de programació i llibreries tindran una manera anàloga de
generar aquests tokens.
</details>

[jwt]: https://jwt.io/
[jose]: https://python-jose.readthedocs.io/

## API Notable

![Projecte NotaBLE](assets/images/notable.jpg)

!!swagger ddapi.json!!
