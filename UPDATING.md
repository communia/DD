# Updating

It is quite important for operators to check this file before updating their
production instances, as it may contain information that is critical for good
continuity of the services.


# Kinds of breaking changes

## Nextcloud upgrades

Nextcloud upgrades must be carried out in a very specific order, if you follow
DD closely you should not have issues with this on 2023 and beyond.

However, if you run an older instance, please find the
**2022-12 - Nextcloud 24** section on this document, and follow the commands
closely.

Further major upgrades will be documented here too, but they should have
little to no impact.


# Important changes in reverse chronological order

## 2023

### 2023-02 - Nextcloud 24.0.10

Update version to 24.0.10

Upgrade from a commit that includes Nextcloud major version 24 (see previous
UPDATING messages) with:

    # Upgrade to NC version 24.0.10  (latest minor on 2023-02-28)
    ./dd-ctl update 65b90724bd45bc34e19c7c652ea1e085f52f92ab


## 2022

### 2022-12 - Nextcloud 24

Starting with commits in december, DD's blessed Nextcloud version is 24.0.8.

Work is already being carried out to upgrade this to major version 25.

In order to safely upgrade to major version 24, the upgrade must be performed
step-wise:

    # Latest commit with NC major version 21 by default
    ./dd-ctl update db4a6d14e9a05ee1b0334aaf87af31703a4ae4d0
    # Upgrade to NC version 22
    ./dd-ctl update cbb4e06e896da40430d66435589ab5ea8cf71420
    # Upgrade to NC version 23
    ./dd-ctl update 57c87d7172579b8fbe0d8249f47ee0966621e252
    # Upgrade to NC version 24.0.8  (latest minor on december 2022)
    ./dd-ctl update f55e830e2cd3071aceb604642fcfb5fdd8d5df17
    #
    # Upgrade to latest state of DD
    # (read more recent notices of important changes before running this)
    #./dd-ctl update
